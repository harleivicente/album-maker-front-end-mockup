A front-end mock-up of an album managing website for desktop/mobile created using Foundation (front-end framework).

To browse mock-ups simply download the folder and open index.html in a browser.

![Log in page.png](https://bitbucket.org/repo/yaB5K8/images/1152124788-Log%20in%20page.png)

![Collection home.png](https://bitbucket.org/repo/yaB5K8/images/3826868130-Collection%20home.png)